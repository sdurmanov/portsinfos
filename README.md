Абонент  488758:
1.	Дано:

- RicoID  488758sv@sevtelecom
- Тех. данные

2. Задача
   -  Просмотр информации по порту аб-та(скорость, с/ш, затухание), информация по слоту коммутатора.
 
3. Алгоритм поиска 
-  Зайти на плату:   telnet  192.168.***.**	
		
user id : ****
password: ****
                 - Просмотр состояния всех портов на плате: 
mBAN> show dsl port
                 - Просмотр состояния порта абонента: порт 29 
mBAN>  show dsl port 29	
  

               - Перезагрузка порта: 
mBAN>  reset dsl port 29
               - Необходимые параметры для оценки порта для оператора:
mBAN>  show dsl port 29
Port                               29
Profile Name           N320-2464/2464-12896AnnexM   
DS Data Rate AS0                 9152 kbit/s             US Data Rate LS0                896 kbit/s    
DS Attenuation                      35 dB                      US Attenuation                   19 dB            
DS SNR Margin                        8 dB                       US SNR Margin                    10 dB   

  
Команды для работы с платами Iskratel:
Команда	Назначение
Show dsl port 	Просмотр всех портов на плате
Show dsl port *	Просмотр состояния порта * (скорость, затухание, с/ш)
Reset dsl port *	Перезагрузка порта *
Set dsl port * port_equp unequipped	Закрыть порт
Set dsl port * port_equp equipped	Открыть порт
Show dsl  profile	Просмотр всех существующих профилей на плате
Set dsl port * profile *	Изменение профиля на порту *
Show vlan member	Проверка вланов










